# Text Recognizer

Text Recognizer.


## Installation

You need install python 3.9 and flask:

```
pip install python
pip install Flask
```

## Important import
```
import Flask
import request
import os
import redirect
import render_template

```

## Getting started

App strat:

```
python app.py

```
## Technologies used

- Python
- JetBrains
- Flask (rest framework)
- JSON

## Description

Main function.
```
@app.route('/', methods=['GET', 'POST'])
```
This function allows to upload a picture in application and recognize text in pictures.
After that it automatically saves this text in .txt file

Main setting > Choose language.


## Author

This app was done by Dmitry Tsunaev.

- [linkedin](http://linkedin.com/in/dmitry-tsunaev-530006aa)

